import yaml from 'js-yaml';
import fs from "fs";

function getYaml(filename: string) {
    return yaml.load(fs.readFileSync(`${process.cwd()}/_content/${filename}`, 'utf-8'));
}

export function getHomepageContent() {
    return getYaml('home.yaml');
}

export function getAboutContent() {
    return getYaml('about.yaml');
}

export function getBlogIndexContent() {
    return getYaml('blog.yaml');
}

export function getProjectsIndexContent() {
    return getYaml('projects.yaml');
}
