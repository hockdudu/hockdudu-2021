import ImageInfo from "../types/ImageInfo";
import sharp from "sharp";

export default async function getImageInfo(imagePath: string, alt?: string): Promise<ImageInfo> {
    const image = sharp(`${process.cwd()}/public/${imagePath}`);

    const metadata = await image.metadata();
    const thumbnail = await image
        .resize({
            fit: 'inside',
            height: 8,
            width: 8
        })
        .removeAlpha()
        .toFormat('webp')
        .toBuffer();

    return {
        alt: alt ?? '',
        height: metadata.height!,
        src: imagePath,
        placeholder: bufferToDataUri(thumbnail, 'image/webp'),
        width: metadata.width!
    };
}

function bufferToDataUri(buffer: Buffer, mediaType: string): string {
    return `data:${mediaType};base64,${buffer.toString('base64')}`;
}
