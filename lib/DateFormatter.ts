const formatterDate = new Intl.DateTimeFormat('en-ch', {
    dateStyle: 'medium',
});

const formatterMonth = new Intl.DateTimeFormat('en-ch', {
    month: 'long',
    year: 'numeric',
});

function getFormattedDate(date: Date): string {
    return formatterDate.format(date);
}

function getFormattedMonth(date: Date): string {
    return formatterMonth.format(date);
}

const DateFormatter = { getFormattedDate, getFormattedMonth }

export default DateFormatter;
