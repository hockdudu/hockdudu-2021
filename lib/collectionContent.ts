import fs from 'fs';
import matter from "gray-matter";
import BlogEntry from "../types/BlogEntry";
import BaseEntry from "../types/BaseEntry";
import ProjectEntry from "../types/ProjectEntry";

const blogDirectory = `${process.cwd()}/_content/blog/`;
const projectDirectory = `${process.cwd()}/_content/projects/`;

function getSlugs(sourceDirectory: string): string[] {
    return fs.readdirSync(sourceDirectory).map(slug => slug.replace(/\.md$/, ''));
}

function getBySlug(slug: string, sourceDirectory: string): BaseEntry {
    const fullPath = `${sourceDirectory}/${slug}.md`;
    const fileContents = fs.readFileSync(fullPath, 'utf8');

    const {data, content} = matter(fileContents);

    return {
        content: content,
        date: data['date'],
        excerpt: data['excerpt'],
        slug: slug,
        title: data['title'],
    }
}

function getAll(sourceDirectory: string): BaseEntry[] {
    const slugs = getSlugs(sourceDirectory);
    return slugs.map(slug => {
        return getBySlug(slug, sourceDirectory);
    }).sort((post1, post2) => {
        return post1.date > post2.date ? -1 : 1;
    });
}

export function getBlogPostBySlug(slug: string): BlogEntry {
    return getBySlug(slug, blogDirectory);
}

export function getAllBlogPosts(): BlogEntry[] {
    return getAll(blogDirectory);
}

export function getProjectPostBySlug(slug: string): ProjectEntry {
    return getBySlug(slug, projectDirectory);
}

export function getAllProjectPosts(): ProjectEntry[] {
    return getAll(projectDirectory);
}
