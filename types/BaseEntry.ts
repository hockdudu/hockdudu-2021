type BaseEntry = {
    slug: string;
    title: string;
    date: string;
    excerpt: string;
    content: string;
}

export default BaseEntry;
