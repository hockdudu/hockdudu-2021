type ImageInfo = {
    alt: string;
    height: number;
    placeholder?: string;
    src: string;
    width: number;
}

export default ImageInfo;
