import ImageInfo from "./ImageInfo";
import TechInfo from "./TechInfo";

type BusinessCardInfo = {
    avatar: ImageInfo;
    name: string;
    email: string;
    profession: string;
    residence: string;
    techStack: TechInfo[];
}

export default BusinessCardInfo;
