type TechInfo = {
    align: string;
    color: string;
    link: string;
    name: string;
    size: number;
    tint: string;
}

export default TechInfo;
