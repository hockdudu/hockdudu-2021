import {GetStaticPaths, GetStaticProps, NextPage} from "next";
import Head from "next/head";
import {getAllProjectPosts, getProjectPostBySlug} from "../../lib/collectionContent";
import ProjectEntry from "../../types/ProjectEntry";
import MainLayout from "../../components/template/MainLayout";
import ArticleHead, {DateFormat} from "../../components/organism/ArticleHead";
import ArticleBody from "../../components/organism/ArticleBody";

type Props = {
    post: ProjectEntry;
}

const BlogPost: NextPage<Props> = ({ post }) => {
    return (
        <MainLayout dark>
            <Head>
                <title>{post.title} · hockdudu.ch</title>
                <meta name="description" content={post.excerpt}/>
            </Head>

            <article>
                <ArticleHead article={post} dateFormat={DateFormat.MonthYear} />
                <ArticleBody article={post} />
            </article>
        </MainLayout>
    );
}

export default BlogPost;

type Params = {
    slug: string;
}

export const getStaticProps: GetStaticProps<Props, Params> = async ({ params }) => {
    const post = getProjectPostBySlug(params!.slug);

    return {
        props: {
            post,
        },
    };
}

export const getStaticPaths: GetStaticPaths = async () => {
    const posts = getAllProjectPosts();

    return {
        paths: posts.map((post) => {
            return {
                params: {
                    slug: post.slug,
                },
            };
        }),
        fallback: false,
    };
}

