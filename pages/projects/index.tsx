import {GetStaticProps, NextPage} from "next";
import Head from "next/head";
import {getAllProjectPosts} from "../../lib/collectionContent";
import ProjectEntry from "../../types/ProjectEntry";
import MainLayout from "../../components/template/MainLayout";
import ProjectCardGroup from "../../components/organism/ProjectCardGroup";
import {getProjectsIndexContent} from "../../lib/singleContent";
import styles from './index.module.scss';
import nl2br from "../../lib/nl2br";

type Props = {
    posts: ProjectEntry[];
    projectsContent: any;
}

const ProjectsIndex: NextPage<Props> = ({ posts, projectsContent }) => {
    return (
        <MainLayout dark>
            <Head>
                <title>Projects · hockdudu.ch</title>
                <meta name="description" content={projectsContent.description} />
            </Head>

            <div className={styles.introduction}>
                <h1>{projectsContent.title}</h1>
                <p>{nl2br(projectsContent.description)}</p>
            </div>
            <ProjectCardGroup projectEntries={posts} />
        </MainLayout>
    )
}

export const getStaticProps: GetStaticProps<Props> = () => {
    const posts = getAllProjectPosts();
    const projectsContent = getProjectsIndexContent();

    return {
        props: {
            posts: posts,
            projectsContent,
        }
    }
}

export default ProjectsIndex;
