import {GetStaticProps, NextPage} from "next";
import MainLayout from "../components/template/MainLayout";
import Head from "next/head";
import {getAboutContent} from "../lib/singleContent";
import markdownToHtml from "../lib/markdownToHtml";
import getImageInfo from "../lib/getImageInfo";
import styles from './about.module.scss';
import BusinessCard from "../components/organism/BusinessCard";
import BusinessCardInfo from "../types/BusinessCardInfo";

type Props = {
    businessCard: BusinessCardInfo;
    content: string;
    description: string;
    title: string;
}

const About: NextPage<Props> = ({ businessCard, content, description, title }: Props) => {
    return (
        <MainLayout>
            <Head>
                <title>About · hockdudu.ch</title>
                <meta name="description" content={description} />
            </Head>

            <div className={styles.businessCard}>
                <BusinessCard businessCardInfo={businessCard} />
            </div>

            <h1>{title}</h1>
            <div className={styles.content} dangerouslySetInnerHTML={{__html: content}} />
        </MainLayout>
    )
}

export default About;

export const getStaticProps: GetStaticProps<Props> = async () => {
    const aboutContent = getAboutContent();
    // @ts-ignore
    const parsedContent = await markdownToHtml(aboutContent.content);
    // @ts-ignore
    const businessCardAvatar = await getImageInfo(aboutContent.businessCard.avatar.src, aboutContent.businessCard.avatar.alt);
    // @ts-ignore
    const businessCard = {...aboutContent.businessCard, avatar: businessCardAvatar} as BusinessCardInfo;

    return {
        props: {
            content: parsedContent,
            // @ts-ignore
            description: aboutContent.description,
            businessCard,
            // @ts-ignore
            title: aboutContent.title,
        }
    }
}
