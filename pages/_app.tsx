import '../styles/globals.scss'
import type { AppProps } from 'next/app'
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <>
            <Head>
                <link rel="icon" href="/favicon.svg" type="image/svg+xml" />
                <title>hockdudu.ch</title>
                <script defer data-domain="hockdudu.ch" data-api="/plsbl/api/event" src="/plsbl/js/script.js"/>
            </Head>
            <Component {...pageProps} />
        </>
    )
}

export default MyApp;
