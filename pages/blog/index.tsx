import {GetStaticProps, NextPage} from "next";
import Head from "next/head";
import BlogEntry from "../../types/BlogEntry";
import {getAllBlogPosts} from "../../lib/collectionContent";
import BlogCardGroup from "../../components/organism/BlogCardGroup";
import MainLayout from "../../components/template/MainLayout";
import {getBlogIndexContent} from "../../lib/singleContent";
import nl2br from "../../lib/nl2br";
import styles from './index.module.scss';

type Props = {
    blogContents: any;
    posts: BlogEntry[];
}

const BlogIndex: NextPage<Props> = ({ blogContents, posts }) => {
    return (
        <MainLayout>
            <Head>
                <title>Blog · hockdudu.ch</title>
                <meta name="description" content={blogContents.description} />
            </Head>

            <div className={styles.introduction}>
                <h1>{blogContents.title}</h1>
                <p>{nl2br(blogContents.description)}</p>
            </div>
            <BlogCardGroup blogEntries={posts} groupByYear />
        </MainLayout>
    )
}

export const getStaticProps: GetStaticProps<Props> = () => {
    const posts = getAllBlogPosts();
    const blogContents = getBlogIndexContent();

    return {
        props: {
            posts,
            blogContents,
        }
    }
}

export default BlogIndex;
