import {GetStaticPaths, GetStaticProps, NextPage} from "next";
import Head from "next/head";
import BlogEntry from "../../types/BlogEntry";
import {getAllBlogPosts, getBlogPostBySlug} from "../../lib/collectionContent";
import MainLayout from "../../components/template/MainLayout";
import ArticleHead, {DateFormat} from "../../components/organism/ArticleHead";
import ArticleBody from "../../components/organism/ArticleBody";

type Props = {
    post: BlogEntry;
}

const BlogPost: NextPage<Props> = ({ post }) => {
    return (
        <MainLayout>
            <Head>
                <title>{post.title} · hockdudu.ch</title>
                <meta name="description" content={post.excerpt}/>
            </Head>

            <article>
                <ArticleHead article={post} dateFormat={DateFormat.Full} />
                <ArticleBody article={post} />
            </article>
        </MainLayout>
    );
}

export default BlogPost;

type Params = {
    slug: string;
}

export const getStaticProps: GetStaticProps<Props, Params> = async ({ params }) => {
    const post = getBlogPostBySlug(params!.slug);

    return {
        props: {
            post,
        },
    };
}

export const getStaticPaths: GetStaticPaths = async () => {
    const posts = getAllBlogPosts();

    return {
        paths: posts.map((post) => {
            return {
                params: {
                    slug: post.slug,
                },
            };
        }),
        fallback: false,
    };
}

