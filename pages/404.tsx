import {NextPage} from "next";
import MainLayout from "../components/template/MainLayout";
import Head from "next/head";
import Link from 'next/link';

const Custom404: NextPage = () => {
    return (
        <MainLayout>
            <Head>
                <title>404: Not found · hockdudu.ch</title>
            </Head>

            <h1>404: Not found</h1>
            <p> Oh, no! It looks like the page you were looking for couldn&apos;t be found. It could&apos;ve been deleted or moved somewhere else.</p>
            <p>Sorry for any inconvenience. I wish you a nice day!</p>
            <Link href="/">Back to home page</Link>
        </MainLayout>
    )
}

export default Custom404;
