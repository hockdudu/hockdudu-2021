import type {GetStaticProps, NextPage} from 'next'
import Head from 'next/head'
import BlogCardGroup from "../components/organism/BlogCardGroup";
import Hero from "../components/organism/Hero";
import ProjectCardGroup from "../components/organism/ProjectCardGroup";
import BlogEntry from "../types/BlogEntry";
import ProjectEntry from "../types/ProjectEntry";
import styles from './index.module.scss';
import {getAllBlogPosts, getAllProjectPosts} from "../lib/collectionContent";
import {getHomepageContent} from "../lib/singleContent";
import EmptyLayout from "../components/template/EmptyLayout";
import nl2br from "../lib/nl2br";

type Props = {
    blogEntries: BlogEntry[],
    projectEntries: ProjectEntry[],
    hasMoreProjects: boolean;
    hasMoreBlog: boolean;
    homeContent: any,
}

const Home: NextPage<Props> = ({ blogEntries, projectEntries, hasMoreProjects, hasMoreBlog, homeContent }: Props) => {
  return (
    <EmptyLayout>
      <Head>
        <title>Home · hockdudu.ch</title>
        <meta name="description" content={homeContent.teaser.content} />
      </Head>

      <div>
        <Hero
            description={homeContent.teaser.content}
            title={homeContent.teaser.title}
        />
      </div>
      <div className={`${styles.section}`}>
        <div className='container-md'>
            <div className={styles.sectionIntroduction}>
                <h2>{homeContent.blog.title}</h2>
                <p>{nl2br(homeContent.blog.description)}</p>
            </div>
            <BlogCardGroup blogEntries={blogEntries} showMore={hasMoreBlog} />
        </div>
      </div>
      <div className={`${styles.section} ${styles.projects} dark-ui`}>
          <div className='container-md'>
              <div className={styles.sectionIntroduction}>
                  <h2>{homeContent.projects.title}</h2>
                  <p>{nl2br(homeContent.projects.description)}</p>
              </div>
              <ProjectCardGroup projectEntries={projectEntries} showMore={hasMoreProjects} />
          </div>
      </div>
    </EmptyLayout>
  )
}

export const getStaticProps: GetStaticProps<Props> = async () => {
    const blogEntries: BlogEntry[] = getAllBlogPosts();
    const projectEntries: ProjectEntry[] = getAllProjectPosts();
    const homeContent = getHomepageContent();

    const visibleBlog = blogEntries.slice(0, 4);
    const visibleProjects = projectEntries.slice(0, 4);

    const hasMoreBlog = visibleBlog.length < blogEntries.length;
    const hasMoreProjects = visibleProjects.length < projectEntries.length;

    return {
        props: {
            blogEntries: visibleBlog,
            projectEntries: visibleProjects,
            homeContent,
            hasMoreBlog,
            hasMoreProjects,
        },
    };
}

export default Home
