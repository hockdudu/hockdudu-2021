import BusinessCardInfo from "../../types/BusinessCardInfo";
import styles from './BusinessCard.module.scss';
import LazyImage from "../atom/LazyImage";
import {SiArchlinux, SiDocker, SiKotlin, SiPhp, SiTypescript} from "react-icons/si";
import Link from 'next/link';
import {IconType} from "react-icons";

type Props = {
    businessCardInfo: BusinessCardInfo;
}

const BusinessCard = ({ businessCardInfo }: Props) => {
    return (
        <div className={`${styles.businessCard} dark-ui`}>
            <div className={`${styles.avatarWrapper} dark-ui`}>
                <div className={styles.avatar}>
                    <LazyImage
                        alt={businessCardInfo.avatar.alt}
                        height={12 * 16} // 12rem
                        src={businessCardInfo.avatar.src}
                        width={12 * 16} // 12rem
                    />
                </div>
            </div>
            <address className={styles.info}>
                <div className={styles.infoHeader}>
                    <div className={`${styles.name} header-font`}>{businessCardInfo.name}</div>
                    <div className={styles.profession}>{businessCardInfo.profession}</div>
                </div>

                <div className={styles.infoContent}>
                    <div><b>Email:</b> {businessCardInfo.email}</div>
                    <div><b>Residence:</b> {businessCardInfo.residence}</div>
                </div>

                <div className={`${styles.techStack} light-ui`}>
                    {businessCardInfo.techStack.map(techItem => {
                        const TechIcon = getIconType(techItem.name);

                        return (
                            <Link key={techItem.name} href={techItem.link} passHref>
                                <a
                                    className={styles.techItem}
                                    rel="noreferrer"
                                    style={{
                                        color: 'inherit',
                                        backgroundColor: techItem.color,
                                        ['--tint-color' as any]: techItem.tint,
                                    }}
                                    target="_blank"
                                    title={techItem.name}
                                >
                                    <TechIcon
                                        size={`${techItem.size}rem`}
                                        title={techItem.name}
                                        style={{
                                            transform: `translate(${techItem.align})`
                                        }}
                                    />
                                </a>
                            </Link>
                        );
                    })}
                </div>
            </address>
        </div>
    )
};

function getIconType(name: string): IconType
{
    switch (name) {
        case 'TypeScript':
            return SiTypescript;
        case 'PHP':
            return SiPhp;
        case 'Arch Linux':
            return SiArchlinux;
        case 'Docker':
            return SiDocker;
        case 'Kotlin':
            return SiKotlin;
        default:
            throw new Error(`Unknown logo named "${name}"`);
    }
}

export default BusinessCard;
