import styles from './Hero.module.scss';
import {useEffect, useRef} from "react";
import nl2br from "../../lib/nl2br";

type Props = {
    description: string;
    title: string;
}

const Hero = ({ description, title }: Props) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        const canvas = canvasRef.current!
        const ctx = canvas.getContext('2d')!;
        const imageData = ctx!.getImageData(0, 0, canvas.width, canvas.height);
        let frame = requestAnimationFrame(loop);

        let startTime: number;

        function loop(originalTime: number) {
            if (startTime === undefined) {
                startTime = originalTime;
            }

            // Always start first execution with time 0
            const t = originalTime - startTime;

            frame = requestAnimationFrame(loop);

            for (let p = 0; p < imageData.data.length; p += 4) {
                const i = p / 4;

                const x = i % canvas.width;
                const y = i / canvas.width >>> 0;

                const r = 0;
                const g = 64 + 128 * (x / canvas.width) + 16 * (Math.sin(t / 1000));
                const b = 64 + 128 * (y / canvas.height) + 16 * (Math.cos(t / 1000));

                imageData.data[p] = r;
                imageData.data[p+1] = g;
                imageData.data[p+2] = b;
                imageData.data[p+3] = 255;
            }

            ctx.putImageData(imageData, 0, 0);
        }

        return () => {
            cancelAnimationFrame(frame);
        }
    }, []);


    return (
        <div className={`${styles.hero} dark-ui`}>
            <canvas className={styles.heroCanvas}
                ref={canvasRef}
                height='64'
                width='256'
            />
            <div className={`${styles.heroContent} container-md`}>
                <h1>{title}</h1>
                <p className={styles.description}>{nl2br(description)}</p>
            </div>
        </div>
    );
}

export default Hero;
