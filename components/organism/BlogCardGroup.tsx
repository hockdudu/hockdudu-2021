import React from 'react';
import Link from 'next/link';
import BlogEntry from "../../types/BlogEntry";
import styles from './BlogCardGroup.module.scss';
import BlogCard from "../molecule/BlogCard";

type Props = {
    blogEntries: BlogEntry[];
    groupByYear?: boolean;
    showMore?: boolean;
}

const BlogCardGroup = ({ blogEntries, groupByYear = false, showMore = false }: Props) => {
    let groups: Map<number, BlogEntry[]> = new Map();
    if (groupByYear) {
        blogEntries.forEach(project => {
            const year = (new Date(project.date)).getFullYear()
            if (!groups.has(year)) {
                groups.set(year, []);
            }

            groups.get(year)!.push(project);
        });
    }

    return (
        <div>
            <div className={`${styles.grid} ${showMore ? styles.hasMargin : ''}`}>
                {groupByYear ? (
                    Array.from(groups.entries()).map(([year, entries]) => (
                        <React.Fragment key={year}>
                            <h3 className={styles.year}>{year}</h3>
                            {entries.map(entry => (
                                <BlogCard key={entry.slug} entry={entry} />
                            ))}
                        </React.Fragment>
                    ))
                ) : (
                    blogEntries.map(entry => (
                        <BlogCard key={entry.slug} entry={entry} />
                    ))
                )}

            </div>
            {showMore && (
                <Link href='/blog' passHref>
                    <a className={styles.seeMore}>
                        Fancy some more? See all blog articles!
                    </a>
                </Link>
            )}
        </div>
    );
};

export default BlogCardGroup;
