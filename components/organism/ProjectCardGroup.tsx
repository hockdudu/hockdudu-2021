import Link from 'next/link';
import ProjectCard from "../molecule/ProjectCard";
import ProjectEntry from "../../types/ProjectEntry";
import styles from './ProjectCardGroup.module.scss';

type Props = {
    projectEntries: ProjectEntry[];
    showMore?: boolean;
}

const ProjectCardGroup = ({ projectEntries, showMore = false }: Props) => {
    return (
        <div>
            <div className={`${styles.grid} ${showMore ? styles.hasMargin : ''}`}>
                {projectEntries.map(entry => (
                    <ProjectCard key={entry.slug} entry={entry}/>
                ))}
            </div>

            {showMore && (
                <Link href='/projects/'>
                    <a className={styles.seeMore}>
                        Do you like what you see? Discover more nice projects!
                    </a>
                </Link>
            )}
        </div>
    )
}

export default ProjectCardGroup;
