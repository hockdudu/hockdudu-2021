import Link from 'next/link';
import styles from './Header.module.scss';
import Button from "../atom/Button";
import Logo from "../atom/Logo";
import BurgerIcon from "../atom/BurgerIcon";
import {useRef, useState} from "react";

const links: string[][] = [
    ['/', 'Home'],
    ['/blog', 'Blog'],
    ['/projects', 'Projects'],
    ['/about', 'About']
]

const Header = () => {
    const mobileNavigationRef = useRef<HTMLDivElement>(null);
    const [isMobileNavigationOpen, setMobileNavigationOpen] = useState(false);

    const onClickBurger = () => {
        const height = mobileNavigationRef.current!.scrollHeight;
        mobileNavigationRef.current!.animate({
            height: isMobileNavigationOpen ? '0px' : `${height}px`,
        }, {
            duration: 200,
            easing: 'ease-out',
            fill: 'forwards',
        });

        setMobileNavigationOpen(isOpen => !isOpen);
    }

    const navigationButtons = links.map(link => (
        <Button
            className={styles.navigationButton}
            href={link[0]}
            key={link[0]}
        >
            {link[1]}
        </Button>
    ));

    return (
        <header className={`${styles.headerWrapper} dark-ui`}>
            <div className={`${styles.header} container-md`}>
                <Link href='/' passHref>
                    <a className={`${styles.logo} header-font`}>
                        <Logo />
                    </a>
                </Link>
                <div className={styles.navigation}>
                    {navigationButtons}
                </div>
                <div
                    aria-label='Toggle menu'
                    className={styles.burger}
                    onClick={onClickBurger}
                    role='button'
                >
                    <BurgerIcon />
                </div>
                <div
                    ref={mobileNavigationRef}
                    className={styles.navigationMobile}
                >
                    {navigationButtons}
                </div>
            </div>
        </header>
    )
}

export default Header;
