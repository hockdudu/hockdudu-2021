import BaseEntry from "../../types/BaseEntry";
import DateFormatter from "../../lib/DateFormatter";
import styles from './ArticleHead.module.scss';
// @ts-ignore
import readingTime from 'reading-time/lib/reading-time';

type Props = {
    article: BaseEntry;
    dateFormat: DateFormat;
}

export enum DateFormat {
    Full,
    MonthYear
}

const ArticleHead = ({ article, dateFormat }: Props) => {
    const formattedDate = getFormattedDate(article.date, dateFormat);
    const readTime = readingTime(article.content);

    return (
        <div className={styles.articleHead}>
            <h1>{article.title}</h1>
            <span className={styles.meta}>
                <time dateTime={article.date}>{formattedDate}</time>
                <span>{readTime.text}</span>
            </span>

        </div>
    )
};

function getFormattedDate(date: string, dateFormat: DateFormat): string {
    switch (dateFormat) {
        case DateFormat.Full:
            return DateFormatter.getFormattedDate(new Date(date));
        case DateFormat.MonthYear:
            return DateFormatter.getFormattedMonth(new Date(date));
    }
}

export default ArticleHead;
