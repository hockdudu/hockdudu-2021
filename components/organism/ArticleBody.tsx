import BaseEntry from "../../types/BaseEntry";
import ReactMarkdown from "react-markdown";
import Link from 'next/link';
import remarkGfm from 'remark-gfm/index';
import rehypeSlug from 'rehype-slug';
import styles from './ArticleBody.module.scss';
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import {darcula} from 'react-syntax-highlighter/dist/cjs/styles/prism';
import {ReactNode} from "react";
import {HiExternalLink} from "react-icons/hi";
import remarkSmartypants from "remark-smartypants";

type Props = {
    article: BaseEntry;
}

const ArticleBody = ({ article }: Props) => {
    return (
        <ReactMarkdown
            className={styles.articleBody}
            components={{
                a: ({node, children: originalChildren, ...props}) => {
                    let children: ReactNode = originalChildren;
                    if (props.role === 'doc-backlink') {
                        children = '\u21A9\uFE0E';
                    }

                    const isRelative = props.href?.startsWith('/');
                    const isFragment = props.href?.startsWith('#');

                    if (isFragment) {
                        return (
                            <a {...props}>{children}</a>
                        )
                    }

                    return (
                        <Link href={props.href!} passHref>
                            <a
                                target={isRelative ? '_self' : '_blank'}
                                rel={isRelative ? '' : 'noreferrer'}
                                {...props}
                            >
                                {children}
                                {!isRelative && (
                                    <HiExternalLink
                                        className={styles.externalIcon}
                                    />
                                )}
                            </a>
                        </Link>
                    )
                },
                code: ({node, inline, className = '', children, ...props}) => {
                    const match = className.match(/language-(?<language>\w+)/);

                    if (!inline && match !== null) {
                        return (
                            <SyntaxHighlighter
                                style={darcula}
                                language={match.groups!['language']}
                            >
                                {String(children).trim()}
                            </SyntaxHighlighter>
                        )
                    }

                    return (
                        <code className={className} {...props}>
                            {children}
                        </code>
                    )
                },
            }}
            remarkPlugins={[remarkGfm, remarkSmartypants]}
            rehypePlugins={[rehypeSlug]}
        >
            {article.content}
        </ReactMarkdown>
    )
}
export default ArticleBody;
