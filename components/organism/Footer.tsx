import styles from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={`${styles.footer} header-font dark-ui`}>
            <div className={`${styles.footerContent} container-md`}>
                <div className={styles.footerElement}>
                    Made with love in Switzerland by Eduardo Rocha.
                </div>
                <div className={styles.footerElement}>
                    <a
                        className={styles.link}
                        href="https://gitlab.com/hockdudu"
                        target="_blank"
                        rel="noreferrer"
                    >Find me at GitLab</a>
                    <span className={styles.spacer} />
                    <a
                        className={styles.link}
                        href="https://www.buymeacoffee.com/hockdudu"
                        target="_blank"
                        rel="noreferrer"
                    >Buy me a coffee</a>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
