import Link from 'next/link';
import BlogEntry from "../../types/BlogEntry";
import styles from './BlogCard.module.scss';
import DateFormatter from "../../lib/DateFormatter";

type Props = {
    className?: string;
    entry: BlogEntry;
}

const BlogCard = ({ className = '', entry }: Props) => {
    const formattedDate = DateFormatter.getFormattedDate(new Date(entry.date));

    return (
        <Link href={`/blog/${entry.slug}`} passHref>
            <a className={`${styles.card} ${className}`}>
                <h3>{entry.title}</h3>
                <time className={styles.date} dateTime={entry.date}>{formattedDate}</time>
                <div>{entry.excerpt}</div>
            </a>
        </Link>
    )
}

export default BlogCard;
