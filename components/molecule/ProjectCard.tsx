import Link from 'next/link';
import ProjectEntry from "../../types/ProjectEntry";
import styles from './ProjectCard.module.scss';
import DateFormatter from "../../lib/DateFormatter";

type Props = {
    entry: ProjectEntry;
}

const ProjectCard = ({ entry }: Props) => {
    const formattedDate = DateFormatter.getFormattedMonth(new Date(entry.date));

    return (
        <Link href={`/projects/${entry.slug}`} passHref>
            <a className={styles.card}>
                <h3>{entry.title}</h3>
                <time className={styles.date} dateTime={entry.date}>{formattedDate}</time>
                <div>{entry.excerpt}</div>
            </a>
        </Link>
    );
}

export default ProjectCard;
