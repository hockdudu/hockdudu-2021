import {ReactNode} from "react";
import Header from "../organism/Header";
import Footer from "../organism/Footer";

type Props = {
    children: ReactNode
}

const EmptyLayout = ({ children }: Props) => {
    return (
        <>
            <Header/>
            <main>
                {children}
            </main>
            <Footer />
        </>
    );
}

export default EmptyLayout;
