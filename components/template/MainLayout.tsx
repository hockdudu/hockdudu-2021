import {ReactNode} from "react";
import Header from "../organism/Header";
import styles from './MainLayout.module.scss';
import Footer from "../organism/Footer";

type Props = {
    children: ReactNode
    dark?: boolean;
}

const MainLayout = ({ children, dark = false }: Props) => {
    return (
        <div className={`${styles.wrapper} ${dark ? 'dark-ui' : ''}`}>
            <Header/>
            <main className={`${styles.main} container-md`}>
                {children}
            </main>
            <Footer />
            {dark && (
                <style global jsx>{`
                  :root {
                    color-scheme: dark;
                  }
                `}</style>
            )}
        </div>
    );
}

export default MainLayout;
