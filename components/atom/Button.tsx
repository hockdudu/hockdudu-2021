import Link from 'next/link';
import {ReactChild} from "react";
import styles from './Button.module.scss';

type Props = {
    children: ReactChild;
    className?: string;
    href: string;
    inverse?: boolean;
    target?: string;
}

const Button = ({ children, className = '', href, inverse = false, target = undefined }: Props) => {
    return (
        <Link href={href} passHref>
            <a className={`${styles.button} ${className} header-font`} target={target}>
                <span className={styles.buttonContent}>
                    <span>{children}</span>
                </span>
                <span className={`${styles.buttonContentHover}`} aria-hidden>
                    <span>{children}</span>
                </span>
            </a>
        </Link>
    )
}

export default Button;
