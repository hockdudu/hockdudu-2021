import LogoIcon from "./LogoIcon";
import styles from './Logo.module.scss';
import {useRef} from "react";

const LogoRotateKeyframes: PropertyIndexedKeyframes = {
    transform: ['rotate(-60deg)', 'rotate(0deg)'],
};

const LogoRotateTiming: KeyframeAnimationOptions = {
    duration: 250,
    easing: 'ease-out'
};

const Logo = () => {
    const logoRef = useRef<HTMLDivElement>(null);
    const iconRef = useRef<HTMLDivElement>(null);

    const isAnimationPlaying = useRef(false);
    const isHovering = useRef(false);

    function onMouseOver() {
        if (isHovering.current || isAnimationPlaying.current) {
            return;
        }

        isHovering.current = true;

        const animation = iconRef.current!.animate(LogoRotateKeyframes, LogoRotateTiming);
        animation.addEventListener('finish', () => {
            isAnimationPlaying.current = false;
        });
        isAnimationPlaying.current = true;
    }

    function onMouseLeave() {
        isHovering.current = false;
    }

    return (
        <div ref={logoRef} className={styles.logo} onMouseOver={onMouseOver} onMouseLeave={onMouseLeave}>
            <LogoIcon ref={iconRef} className={styles.logoIcon} />
            <div className={styles.text}>
                Hockdudu
            </div>
        </div>
    )
}

export default Logo;
