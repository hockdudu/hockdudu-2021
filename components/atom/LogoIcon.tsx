import styles from './LogoIcon.module.scss';
import {forwardRef} from "react";

type Props = {
    className?: string;
}

const LogoIcon = forwardRef<HTMLDivElement, Props>(({ className = ''}, ref) => {
    return (
        <div ref={ref} className={className}>
            <svg className={styles.svg} width="549.62" height="630" version="1.1" viewBox="0 0 145.42 166.69" xmlns="http://www.w3.org/2000/svg">
                <g transform="translate(-27.781 -15.875)" fill="none" stroke="currentColor" strokeWidth="7.9375">
                    <path d="m31.75 138.91v-79.375l68.741-39.687 68.741 39.687v79.375l-68.741 39.688z" strokeLinejoin="round"/>
                    <path d="m31.75 59.531 137.48 79.375"/>
                    <path d="m31.75 138.91 137.48-79.375"/>
                    <path d="m100.49 19.844v158.75"/>
                </g>
            </svg>
        </div>
    );
});
LogoIcon.displayName = 'LogoIcon';

export default LogoIcon;
