import Image from 'next/image';
import styles from './LazyImage.module.scss';

type Props = {
    alt: string;
    className?: string;
    height?: number;
    placeholder?: string;
    src: string;
    width?: number;
}

const LazyImage = ({ alt, className = '', height, src, placeholder, width }: Props) => {
    const layout = height !== undefined && width !== undefined ? 'intrinsic' : 'fill';

    return (
        <div className={`${styles.lazyImage} ${className}`}>
            <Image
                alt={alt}
                blurDataURL={placeholder}
                layout={layout}
                height={height}
                src={src}
                placeholder={placeholder !== undefined ? 'blur' : 'empty'}
                width={width}
            />
        </div>
    )
}

export default LazyImage;
