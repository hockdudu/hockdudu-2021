import styles from './BurgerIcon.module.scss';

const BurgerIcon = () => {
    return (
        <svg className={styles.svg} width="10" height="10" version="1.1" viewBox="0 0 10 10" xmlns="http://www.w3.org/2000/svg">
            <g stroke="currentColor" strokeLinecap="round" strokeWidth="1px">
                <path d="m1 2h8"/>
                <path d="m1 5h8"/>
                <path d="m1 8h8"/>
            </g>
        </svg>
    )
}

export default BurgerIcon;
