---
title: "Hello world"
excerpt: "The first blog entry from many to come."
date: "2021-11-12"
---

Hello you!

As you may see, this is the first blog entry in my website.
I've had this idea of writing a blog for a long time, but I never came
to doing it. Now it's here, and it's here to stay!

I plan to write mostly about programming- and informatics-related topics.
Maybe also about other topics, but I haven't decided it yet. We'll see!
