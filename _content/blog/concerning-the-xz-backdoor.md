---
title: "Concerning the XZ backdoor"
excerpt: "The implications of the XZ Utils backdoor — a wake-up call for software security and the health of the open-source ecosystem."
date: "2024-04-11"
---

Well, it's bad. You've probably heard about it already. Around two weeks ago the [XZ backdoor](https://en.wikipedia.org/wiki/XZ_Utils_backdoor) was found by Andres Freund.

If not: In a nutshell, a bad actor worked since 2021 in many small steps to gain trust and gain control as a maintainer of the XZ Utils project -- a compression utility used in one way or another in most, if not all, Linux distributions. The backdoor would allow the attacker to execute arbitrary code on virtually every single Linux server. But luckily the backdoor was found before it could be exploited.

We may have dodged a bullet here. But this sets a precedent. This might not be the first time a backdoor was found in a widely used software, but this case is different. The attacker worked for years to gain trust and control. This is a new level of attack, and it's more difficult to prevent.

The question is: How can we prevent this from happening again? Is it even possible to prevent?

I don't have the answer, and I'm sure a lot of genius people are working hard on solving this.

And another question also arises: Are we completely sure that there's no other backdoor out there on other widely used software? I mean, it's naive to think there's none -- there's surely something somewhere, on an open-source project or not. But it will take a lot of effort to find them. Who's going to do it?

I could try and end this post with a positive note, "We can be glad we were lucky this time", but no. We can't count on luck. We need to be proactive and find measures to prevent this from happening again. Make such attacks impossible...

... and goddamn help maintainers of such projects! Projects that are in use virtually everywhere, but nobody gives them the attention they deserve. It's sad, but the [xkcd 2347](https://xkcd.com/2347/) remains true.

And as long as it's true, events like this will happen.

---

As I'm primarily a web developer, I remember events like [event-stream](https://www.theregister.com/2018/11/26/npm_repo_bitcoin_stealer/) and [colors / faker](https://www.bleepingcomputer.com/news/security/dev-corrupts-npm-libs-colors-and-faker-breaking-thousands-of-apps/), which happened for similar reasons. But the state of frontend packages on the web is a whole another can of worms...

Also, you can find a great timeline of events and more insightful links on [Evan's blog](https://boehs.org/node/everything-i-know-about-the-xz-backdoor).
