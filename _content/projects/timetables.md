---
title: "Timetables"
excerpt: "A simple Android app for keeping track of lessons' time and location."
date: "2017-10"
---

## Description
Picture this: You go to school, and have many lessons you have to attend to. Multiple lessons a day, each in a different classroom. Adding to that, the lessons plan changes twice a year. You just can't memorize it all.

You need a way to look it up efficiently. The school might provide the timetables in an online-service, but this service doesn't work well on mobile phones.

So, what do you do? Yes, you create your own app for this. And so, Timetables was born.

This app allows you to enter each lesson's data in its database, and it shows you the lessons in a nice, sorted list. Adding to that, you can create a shortcut to directly open the next lesson. This way, you'll know which classroom to go to in a single tap. And if you're impatient, the app shows the percentage of the current lesson: In case you can't wait for it to be over.

## History

Originally started as a Java app that downloaded the school's .ics (calendar file format), I soon had the idea of writing an Android app. Writing the app only lasted a few days, and I was able to use it in this form for most of the first school year.

After a while, while reading the Android documentations, I discovered the programming language Kotlin. Let's just say I felt in love with it. Safe, expressive, and developer-friendly &ndash; things I miss in other languages. Anyway, I played around with it for a while and then decided to port the application to Kotlin, as it was a nice opportunity to use Kotlin in a "real application".

Porting it to Kotlin only needed a few days, and I was happy with it. A few months down the line, I added more features and cleaned the code a bit.

But I was still unhappy with the overall app, in particular with the lessons displayed: the info shown wasn't enough, as the .ics fields consisted mostly of abbreviations.

So, I rebuilt the app to use a local database instead of downloading and parsing .ics. For the end user it meant they had to add the records themselves, but it is something that must be done only a few times a year.

And this is more or less where we are today. The app may be simple, but it helped me as a practical way of always knowing where the next lesson was.

## Source code
The project's source code can be found under https://gitlab.com/hockdudu-school/timetables-android-kotlin
