---
title: "dd-player (gibb module 104)" # implement data model (Datenmodell implementieren)
excerpt: "An overkill project that I'm really proud of making."
date: "2018-06"
---

## Description
If you'd ask me, what's the most overkill project I've done for a school module, I'd say it was a project
for the module 104. We had a really simple requirement: Create any program that uses CRUD
(Create, Read, Update and Delete, more about CRUD on
[Wikipedia](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)).

I thought: Well, I can't simply create an app for a database with two tables, that'd be way too easy&hellip;
Hey, why not recreate something like SoundCloud — make a website on which one can upload songs, create playlists and
albums, and clearly register / login? One more thing: Make it extensible, so separate the server and the
client. And don't forget, the page shouldn't reload when navigating the site, otherwise the song would stop
playing. It'd be so easy and it would be _sooo_ much fun!

It wasn't fun. As we needed only to create a simple app, we didn't receive so much time on the school, so I
did most of the work at home: On weekends and at night, because who needs sleep, right? _Right?!_

I was really stressed to finish it on time, I didn't sleep well on those weeks, but I managed to do it. The
grading was so anticlimactic, though. The teacher did really only verify if the four CRUD operations were
there. Needless to say, I got a full grade, and the teacher asked for the source of my code, as I was the
one who has done the most for this module.

It might've been the most stressful module I had (on this school year), but I'm really proud of myself. I've
learned a lot. I've discovered new technologies, stumbled to problems I've never had before and fought my
way to solve them. I'd like to mention two tasks I stumbled upon:

### Converting songs
The whole logic of converting songs wasn't easy. A whole bunch of checks had to be done and the song and the
album art had to be converted to a universal format.

But you can't only send the client a response after converting them. The connection would stay open during
the whole convert process, and this could result into a timeout. So, I created a task handler with polling.
The client would query the convert process status after the upload every couple seconds.

### Single-page application (SPA)
Making the website a single-page application wasn't an easy task, either. Solely the task of making the
templating work on the client took two days to work. Not because it was difficult, but because
it was an entirely new thing for me.

I had to create a whole MVC logic on the client for it to work as it should. Yes, I could've used a library
like AngularJS, but that would've been too easy. I wanted to get my hands dirty and learn how this really
works. And I did. And I'm very proud of myself for doing it.

The templating is done on the client using [nunjucks](https://mozilla.github.io/nunjucks/).
All the page templates are downloaded from the server and those are then used for building the pages,
depending on the URL the user is currently on.

## Source code
As I always do with other projects, this project is open-source. Even though I don't really think many are
going to find it useful, I think it's better for it to be public for all to see. I don't get anything
from keeping it to myself; by sharing it with the world, I might even help someone learn something.

Anyway, below is the code from both the client and the server:

- [Client code](https://gitlab.com/hockdudu-school/school-104-player-client)
- [Server code](https://gitlab.com/hockdudu-school/school-104-player-server)

## Live preview
A live preview of the project isn't available yet. Because users can freely register and upload songs, it
could be a little dangerous, as someone could spam it, as it doesn't have any protection.

I intend to implement a way of creating a self-refreshing server (e.g. with Docker) in the future, though.
