---
title: "üK-arch (vocational course module 101)"
excerpt: "A vocational course's project on which I created a website in one week (the course's duration)."
date: "2018-02"
---

## Description
On the vocation course 101 &ndash; create and publish a website (Webauftritt erstellen und veröffentlichen) &ndash; we had the objective to create a website. The choice of the theme was free, there were only few requirements for the website.

I chose Arch Linux for a theme because it's my favorite Linux distribution, which I have on all of my computers.

As the company I worked on during my apprenticeship does mainly websites, this module was easy for me. Because I love a challenge, I tried technologies I never used before and even created my own gallery with lazy loading and such. Because I wasn't happy with the gallery not working well on mobile devices, after the course ended, I kept working a little bit on the library and tried to make it somewhat mobile friendly.

## Live preview
A live preview of the website can be found under the following links:

- https://uk-arch.hockdudu.ch/ &ndash; the website as it was submitted
- https://uk-arch-beta.hockdudu.ch/ &ndash; the website with the mobile-friendly gallery

## Source code
The project's source code can be found under https://gitlab.com/hockdudu-school/uK-101-arch
