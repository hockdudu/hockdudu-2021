---
title: "Website Redesign 2021"
excerpt: "Remaking my homepage with a new design, new technologies and new features."
date: "2021-11"
---

## Starting point
I've published my first homepage way back in 2019. It was more or less a weekend project, as the whole website was done in four days, and I haven't updated it since.

Now in 2021, an update was long overdue. The design was very simplistic, it didn't show much of what I do, and it lacked a blog feature, something I wanted to have for some time already.

So, I wanted to create a new homepage. In the beginning, I wondered for way too long on how I should make the website, both about the design and about the tech stack. As I had gathered some React experience at my job, I chose to make the website in Next.js, a React framework.

But I have committed a mistake: Starting the project without a well-defined objective and starting coding before having any idea or concept about the design. Unsurprisingly, this first prototype didn't go anywhere.

## Back to the project again
Four months later, I started the project from the ground up. This time with a well-defined idea and design. Things went well at the beginning, but as quickly as I began working on the project, I had to stop it and start all over again.

See, I firstly tried it with SvelteKit, a framework for building websites with Svelte, for the sake of trying something new. This framework is actually good, but I've found myself struggling with it too many times. I had issues on how static pages from dynamic routes were generated. In Next.js, you have the property `getStaticPaths()`, where you state which routes exist for a dynamic route (for example `/blog/[slug]`, where `[slug]` is the dynamic part).[^nextjs-static-paths] In SvelteKit there isn't anything like that. It instead crawls your pages, find links referencing to your page.[^svelte-prerender]

Furthermore, if you want to open files from the filesystem, in SvelteKit you can't do it without creating an API-endpoint first.[^svelte-routing-endpoints][^svelte-auto-endpoints] In Next.js, it's easy as adding the property `getStaticProps()`.[^nextjs-static-props]

To be honest, these issues wouldn't actually hinder my project. I could live with them. But still, it was reason enough for me to go back to Next.js, as this concept in Svelte simply didn't click for me. Maybe someday I might come back, but for now, I'll stick with Next.js.

Yes, the rant is finished now. 😅

## Third time's a charm

So, I converted the components I had written in Svelte to React. This process was actually pretty straightforward, I must add.

For about a month I worked heavily on the website, adding many features, tweaking the design and adding some actual content to it. This time I didn't have any difficulties developing, everything went very smoothly.

And here we are now. Finally, I'm pleased to present you my new website. It doesn't have much content at the moment, but this is of no concert. The website is only going to grow in the next weeks, months and years.

If you wish to take a look, the source code can be found at [GitLab](https://gitlab.com/hockdudu/hockdudu-2021). Go on, don't be shy!

Signing up,  
~Eduardo

[^nextjs-static-paths]: https://nextjs.org/docs/basic-features/data-fetching#getstaticpaths-static-generation
[^svelte-prerender]: https://kit.svelte.dev/docs#ssr-and-javascript-prerender
[^svelte-routing-endpoints]: https://kit.svelte.dev/docs#routing-endpoints
[^svelte-auto-endpoints]: https://github.com/sveltejs/kit/issues/758
[^nextjs-static-props]: https://nextjs.org/docs/basic-features/data-fetching#getstaticprops-static-generation
