---
title: "mpd-radio.fm"
excerpt: "A web interface for MPD to be used as a radio"
date: "2018-12"
---

## Description
This project is a combination of both a client and a server for MPD (Music Player Daemon)[^mpd]. It leverages MPD's ability to play network streams to use is as a network radio.

I created this project in the course of only a few days. Nonetheless, we used it at our office for about a year, and it received very positive feedback. It's only not in operation anymore because the company moved to a new location with an open plan, where such radio doesn't make much sense anymore.

## Source code
The project's source code can be found under https://gitlab.com/hockdudu/mpd-radio.fm

[^mpd]: https://www.musicpd.org/
