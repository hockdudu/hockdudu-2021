/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback = {
        fs: false,
      };
    }

    return config;
  },
  swcMinify: true,
  rewrites: async () => {
    return [
      {
        source: '/plsbl/js/script.js',
        destination: 'https://plausible.io/js/plausible.js'
      },
      {
        source: '/plsbl/api/event',
        destination: 'https://plausible.io/api/event'
      }
    ];
  },
}
